#!/bin/bash
#
# THIS SCRIPT IS BEING RUN UNDER ROOT!!!
#
# [repo.or.cz] You will need to manually update this file if you modify
# it in the repository.

set -e

proj="$1"
groupfile="$2"
mirror_user="$3"
cd "$proj.git"

chmod ug+rw,o+r . -R 2>&1 | (grep -v 'No such file or directory' || true)

[ -e .nofetch ] || exit

xproj="$(echo "$proj" | sed 's/[.\/]/\\&/g')"
gid="$(sed -ne "/^$xproj:/ { s/^[^:]*:[^:]*:\([0-9]*\):.*/\1/; p }" "$groupfile")"
if [ -z "$gid" ]; then
	echo "cannot resolve gid for $proj ($xproj)" >&2
	exit 1
fi

chown "$mirror_user"."$gid" info refs objects -R 2>&1 | (grep -v 'No such file or directory' || true)
