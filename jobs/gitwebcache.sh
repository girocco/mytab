#!/bin/bash

. @basedir@/shlib.sh

set -e

cd "$cfg_cgiroot"

# Re-generate the cache; we must be in same group as cgi user and
# $cache_grpshared must be 1.
# We get rid even of stderr since it's just junk from broken repos.
perl -le 'require("./gitweb.cgi"); END { my @list = git_get_projects_list(); cached_project_list_info(\@list, 1, 1, 1); }' >/dev/null 2>&1
