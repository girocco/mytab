#!/usr/bin/perl
# (c) Petr Baudis <pasky@suse.cz>
# GPLv2

use strict;
use warnings;

use lib ".";
use Girocco::CGI;
use Girocco::Config;


# Ultra-trivial templating engine;
# /^@section=SECTION
# /^@heading=HEADING
# /^@header		produces HTML header based on @section and @heading
# /@@gitweburl@@/	substitute for gitweburl configuration variable


my $pathinfo = $ENV{PATH_INFO};
unless ($pathinfo) {
	my $gcgi = Girocco::CGI->new('HTML Templater');
	print "<p>Hi, this is your friendly HTML templater speaking. Pass me template name.</p>\n";
	exit;
}

unless ($pathinfo !~ m#\./# and open(TEMPLATE, "$Girocco::Config::basedir/html/$pathinfo")) {
	my $gcgi = Girocco::CGI->new('HTML Templater');
	print "<p>Invalid template name.</p>\n";
	exit;
}

if ($pathinfo =~ /\.png$/) {
	print "Content-type: image/png\n\n";
	print while (<TEMPLATE>);
	exit;
}

my ($gcgi, $section, $heading);

while (<TEMPLATE>) {
	chomp;
	if (s/^\@section=//) {
		$section = $_;
		next;
	} elsif (s/^\@heading=//) {
		$heading = $_;
		next;
	} elsif (s/^\@header//) {
		$gcgi = Girocco::CGI->new($heading, $section);
		next;
	} else {
		s/@@(\w+?)@@/${$Girocco::Config::{$1}}/ge;
		print "$_\n";
	}
}

close TEMPLATE;

$gcgi and $gcgi->srcname("html/$pathinfo");
