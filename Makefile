# The target 'all' will do some theoretical pre-processing. The target
# 'install' will put all the bits in their place according to Girocco::Config
# (see INSTALL for details). Set GIROCCO_CONFIG to a different value in order
# to install according to custom config.

all::


install::
	@./install.sh


clean::
