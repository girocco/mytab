Girocco
=======
Petr Baudis <pasky@ucw.cz>


This is the duct tape that ties repo.or.cz together, codenamed Girocco.
It is packaged here for easy re-deployment - just follow the INSTALL file
for brief deployment instructions; you may hit some obstacle here or there,
though, so be prepared.


About Girocco
-------------

Girocco is a Git hosting solution, designed to allow users easily create and
access repositories over the web, either in mirror mode (Girocco maintains a
clone of the given repository locally) or in push mode (Girocco maintains
a secure chroot with ssh inside for push-only access). A well-known feature
is forking support, where users can easily publish modifications of projects
they don't own, and mob user support, allowing a sandbox within a project
where anonymous pushing is allowed.

Typical scenarios are:

* Full-blown hosting site with both mirroring and ssh push access
  (like repo.or.cz)
* Internal Git hosting site with no mirroring, but users being able to easily
  set up projects to push to (corporate deployment with no networked homes)
* Internal Git hosting site with no pushing, instead mirroring specified
  projects from people's home directories for others to easily access and fork
  (corporate deployment especially suitable for NFS homes and UNIX-savvy
  users)

Girocco itself comprises of a terse documentation, somewhat customized gitweb
instance, CGI scripts for user and project management, and several management
scripts for the hosting site maintenance.


Girocco vs...
-------------

* Gitorious: Gitorious is very slick hosting solution, but without mirroring
  support (thus less flexible for both public and corporate deployment) and
  offering different project model and interface nice for Web 2.0 fans but
  not for those who prefer the raw beauty and terseness of gitweb.

* GitHub: Like Gitorious, but actually not open-source at all.

* Gitosis, gitolite: Not really a hosting solution, just a clever way to give
  multiple users push access over SSH without giving them actual UNIX accounts
  on the system. It could be used as an alternative to chroot for push access
  support in Girocco, though that is unfortunately not yet implemented.


Requirements and setup
----------------------

You will need git pre-installed, or you can build and install one from
the git.git subdirectory; normally, Girocco will use only gitweb from there.
However, some bits of Girocco (e.g. the chroot setup) will rely on
/usr/bin/git regardless of cmd_git settings; for chroot you will also need
/bin/nc.openbsd (netcat-openbsd debian package). You will need to add the
right users to the right groups (a dedicated Girocco user and the CGI user,
at least); root access or root user cooperation will be probably essential
unless you have good suexec setup. Girocco is designed to be run only with
single instance within one system. If you want git protocol access, you will
need to set up git-daemon independently, but it's very easy with inetd.

After configuring Girocco (editing Girocco/Config.pm and possibly other
files), you should try to run make install and carefully look at any
errors. See INSTALL for details. The installation procedure (especially
chroot setup) is tuned for Debian systems, elsewhere you might need to
adjust few things.

You should be running jobd.sh at all times - it will periodically check
all the repositories and check if any need garbage collection, but also
update them if the mirroring mode is enabled.

If you enable mirror support or want to have push notifications, you must
also be running taskd.pl - it will listen to clone requests and perform
the actual clone operations, and listen to notification messages from
repository post-receive hooks and perform notifications.

If you enable push support, you will have to run sshd as root from
within the chroot setup by make install; no special setup of the sshd
is necessary, up to specifying a port to listen at in etc/ssh/sshd_config
within the chroot (if the port is not to be 22). You will also have to
enable the permission fixup scripts running as root.

Alternately, you can use push support with extremely relaxed security,
but using the system-wide password database and not requiring a chroot.
Or you could implement Gitosis permission model and send me a patch. ;-)


History and naming
------------------

Until Jul 2008, we called all the repo.or.cz machinery just 'repo', however
that is not very good name, especially since the machinery is now suitable
for universal usage even outside of repo.or.cz.  Thankfully, Jan Engelhart
invented a nice name 'Girocco', standing for 'GIt Repo.Or.Cz COdebase'.

At that time, the machinery was a set of ugly cronjobs completely specific
for repo.or.cz. However, Novartis sponsored an internship for Pasky to
generalize the framework and adapt it for an internally suitable usage.
And another year later, Pasky finally finished the job by porting Girocco
back to useable state for repo.or.cz and further cleaning it up.


Forks
-----

An important Girocco concept is a _project fork_ - anyone can fork any
project (including a fork of another project), the fork resides in the
directory structure as PARENT/FORK.git. Forks are meant as a place for
people to freely publish their modifications for the project, even if
they don't have push permissions to the parent. To save space and
bandwidth, the fork will reuse objects in the parent project, garbage
collection is done in a clever way not to prune objects used in forks.


Implementation of project database
----------------------------------

(All uids and gids are allocated from range 65536..infty. All passwords are
stored in DES crypt(3) format because Apache is moronic - in the past
the group file was also used as htpasswd file.)

When you register a project, it will get a gid allocation and you will set a
password for it. The triple is stored in a group(5) file (but containing just
the project groups):

	projname:crypt:gid:list,of,users

When you register a user, it will get a uid allocation and you will upload
an ssh public key for it. The user is stored in a passwd(5) file (but
containing just the repo.or.cz users; 65534 is nogroup):

	username:x:uid:65534:email:/:/bin/git-shell

The authorized keys are stored in /etc/sshkeys/username.

When you (un)assign user to a project, you just manipulate the list of users
for the project in /etc/group. The web interface for the project administration
is protected by the group password.

Since Apache is not in the project groups, there is a special cronjob run
once in a while to fix up the permissions for the refs/, info/, and objects/
project directories, under the root user.

If the given project is in push mode, that is indicated by having a .nofetch
file in the repository. If the given project is in mirror mode on the other
hand, that is indicated by the absence of .nofetch and by having double colon
after the gid in the group(5) file - this prevents listed users to actually
have write access to the repository.
