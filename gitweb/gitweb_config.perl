# Pull Girocco config
use lib ".";
use Girocco::Config;

## For the complete overview of available configuration options,
## see git.git/gitweb/gitweb.perl file beginning (git.git/gitweb/README
## may miss some custom patches, in theory).

# Whether to include project list on the gitweb front page; 0 means yes,
# 1 means no list but show tag cloud if enabled (all projects still need
# to be scanned, unless the info is cached), 2 means no list and no tag cloud
# (very fast)
our $frontpage_no_project_list = 1;

## projects list cache for busy sites with many projects;
## if you set this to non-zero, it will be used as the cached
## index lifetime in minutes
our $projlist_cache_lifetime = 10;

## default charset for text/plain blob
our $default_text_plain_charset  = 'utf-8';

# Comment out to disable ctags
$feature{ctags}{default}=["$Girocco::Config::webadmurl/tagproj.cgi"];

$feature{blame}{default}=[1];

$feature{'snapshot'}{'default'} = ['tgz', 'zip'];


### You probably don't really want to tweak anything below.

# Base web path
our $my_uri = $Girocco::Config::gitweburl;

## core git executable to use
## this can just be "git" if your webserver has a sensible PATH
our $GIT = $Girocco::Config::git_bin;
#
## absolute fs-path which will be prepended to the project path
our $projectroot = $Girocco::Config::reporoot;
# source of projects list
our $projects_list = $Girocco::Config::chroot."/etc/gitweb.list";
#
## target of the home link on top of all pages
our $home_link = $Girocco::Config::gitweburl;
#
## string of the home link on top of all pages
our $home_link_str = $Girocco::Config::name;
#
## name of your site or organization to appear in page titles
## replace this with something more descriptive for clearer bookmarks
our $site_name = $Girocco::Config::title;
## html text to include at home page
our $home_text = "$Girocco::Config::webroot/indextext.html";
#
## URI of stylesheets
our @stylesheets = ("$Girocco::Config::gitwebfiles/gitweb.css");
## URI of GIT logo (72x27 size)
our $logo = "$Girocco::Config::gitwebfiles/git-logo.png";
## URI of GIT favicon, assumed to be image/png type
our $favicon = "$Girocco::Config::gitwebfiles/git-favicon.png";
## URI of blame.js
our $blamejs = "$Girocco::Config::gitwebfiles/blame.js";
## URI of gitweb.js
our $gitwebjs = "$Girocco::Config::gitwebfiles/gitweb.js";
#
## list of git base URLs used for URL to where fetch project from,
## i.e. full URL is "$git_base_url/$project"
our @git_base_url_list = ();
$Girocco::Config::gitpullurl and push @git_base_url_list, $Girocco::Config::gitpullurl;
$Girocco::Config::httppullurl and push @git_base_url_list, $Girocco::Config::httppullurl;
#
our $git_base_push_url = $Girocco::Config::pushurl;

our $cache_grpshared = 1;

$feature{pathinfo}{default}=[1];

$feature{forks}{default}=[1];

$feature{actions}{default}=[
	('graphiclog', "$Girocco::Config::gitwebfiles/git-browser/by-commit.html?r=%n", 'log'),
	('edit', "$Girocco::Config::webadmurl/editproj.cgi?name=%n", 'tree'),
	('fork', "$Girocco::Config::webadmurl/regproj.cgi?fork=%n", 'edit')
];
