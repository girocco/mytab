#!/bin/bash

. @basedir@/shlib.sh

set -e

proj="$1"
cd "$cfg_reporoot/$proj.git"

if check_interval lastgc $cfg_min_gc_interval; then
	progress "= [$proj] garbage check skip (last at $(config_get lastgc))"
	exit 0
fi
if [ -e .nogc ]; then
	progress "x [$proj] garbage check disabled"
	exit 0
fi
progress "+ [$proj] garbage check (`date`)"

# safe pruning: we put all our objects to all forks, then we can
# safely get rid of extra ones; repacks in forks will get rid of
# the redundant ones again then
forkdir="$1"
if [ -d "../$forkdir" ]; then
	get_repo_list "$forkdir/" |
		while read fork; do
			# Match objects in parent project
			for d in objects/?? objects/pack; do
				[ "$d" != "objects/??" ] || continue
				mkdir -p "$cfg_reporoot/$fork.git/$d"
				ln -f "$d"/* "$cfg_reporoot/$fork.git/$d"
			done
		done
fi

quiet=; [ -n "$show_progress" ] || quiet=-q
git repack -a -d --window-memory=1G -l $quiet
git prune
git update-server-info
config_set lastgc "$(date -R)"

progress "- [$proj] garbage check (`date`)"
