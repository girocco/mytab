#!/usr/bin/perl
#
# taskd - Clone repositories on request
#
# taskd is Girocco mirroring servant; it processes requests for clones
# of given URLs received over its socket.
#
# When a request is received, new process is spawned that sets up
# the repository and reports further progress
# to .clonelog within the repository. In case the clone fails,
# .clone_failed is touched and .clone_in_progress is removed.

# Clone protocol:
# Alice sets up repository and touches .cloning
# Alice opens connection to Bob
# Alice sends project name through the connection
# Bob opens the repository and sends error code if there is a problem
# Bob closes connection
# Alice polls .clonelog in case of success.
# If Alice reads "@OVER@" from .clonelog, it stops polling.

# Ref-change protocol:
# Alice opens connection to Bob
# Alice sends ref-change command for each changed ref
# Alice closes connection
# Bob sends out notifications

# Based on perlipc example.

use strict;
use warnings;

use Girocco::Config;
use Girocco::Notify;
use Girocco::Project;
use Girocco::User;
use Socket;

$| = 1;

sub logmsg { print '['.(scalar localtime)."] $0 $$: @_\n" }

my $NAME = $Girocco::Config::chroot.'/etc/taskd.socket';
my $uaddr = sockaddr_un($NAME);

socket(Server, PF_UNIX, SOCK_STREAM, 0) or die "socket: $!";
unlink($NAME);
bind(Server, $uaddr) or die "bind: $!";
listen(Server, SOMAXCONN) or die "listen: $!";
chmod 0666, $NAME or die "chmod: $!";


use POSIX ":sys_wait_h";
sub REAPER {
	my $child;
	my $waitedpid;
	while (($waitedpid = waitpid(-1, WNOHANG)) > 0) {
		logmsg "reaped $waitedpid" . ($? ? " with exit $?" : '');
	}
	$SIG{CHLD} = \&REAPER;  # loathe sysV
}

$SIG{CHLD} = \&REAPER;  # Apollo 440

sub spawn {
	my $coderef = shift;

	my $pid = fork;
	if (not defined $pid) {
		logmsg "cannot fork: $!";
		return;
	} elsif ($pid) {
		logmsg "begat $pid";
		return; # I'm the parent
	}
	
	$SIG{CHLD} = 'DEFAULT';

	open STDIN, "<&Client" or die "can't dup client to stdin";
	open STDOUT, ">&Client" or die "can't dup client to stdout";
	exit &$coderef();
}

sub clone {
	my ($name) = @_;
	my $proj = Girocco::Project->load($name);
	$proj or die "failed to load project $name";
	$proj->{clone_in_progress} or die "project $name is not marked for cloning";
	$proj->{clone_logged} and die "project $name is already being cloned";
	print STDERR "cloning $name\n";
	open STDOUT, ">".$Girocco::Config::reporoot."/".$name.".git/.clonelog" or die "cannot open clonelog: $!";
	open STDERR, ">&STDOUT";
	open STDIN, "</dev/null";
	exec $Girocco::Config::basedir.'/taskd/clone.sh', "$name.git" or die "exec failed: $!";
}

sub ref_change {
	my ($arg) = @_;
	my ($uid, $name, $oldrev, $newrev, $ref) = split(/\s+/, $arg);

	my $proj = Girocco::Project->load($name);
	$proj or die "failed to load project $name";

	my $user;
	if ($uid >= 0) {
		$user = Girocco::User->load_by_uid($uid);
		$user or die "failed to load user $uid";
	}

	print STDERR "ref-change $uid $name ($ref: $oldrev -> $newrev)\n";
	open STDIN, "</dev/null";
	Girocco::Notify::ref_change($proj, $user, $ref, $oldrev, $newrev);
	return 0;
}

while (1) {
	unless (accept(Client, Server)) {
		logmsg "accept failed: $!";
		next;
	}
	logmsg "connection on $NAME";
	spawn sub {
		my $inp = <>;
		chomp $inp;
		$inp or exit 0; # ignore empty connects
		my ($cmd, $arg) = $inp =~ /^([a-zA-Z0-9-]+)\s+(.*)$/;
		if ($cmd eq 'clone') {
			clone($arg);
		} elsif ($cmd eq 'ref-change') {
			ref_change($arg);
		} else {
			die "unknown command: $cmd";
		}
	};
	close Client;
	sleep 1;
}
