package Girocco::User;

use strict;
use warnings;

use Girocco::Config;

BEGIN {
	use Girocco::CGI;
	use Girocco::Util;
	use Digest::SHA1 qw(sha1_hex);
}

sub _passwd_add {
	my $self = shift;
	filedb_atomic_append(jailed_file('/etc/passwd'),
		join(':', $self->{name}, 'x', '\i', 65534, $self->{email}, '/', '/bin/git-shell'));
}

sub _sshkey_path {
	my $self = shift;
	'/etc/sshkeys/'.$self->{name};
}

sub _sshkey_load {
	my $self = shift;
	open F, "<".jailed_file($self->_sshkey_path) or die "sshkey load failed: $!";
	my @keys;
	my $auth;
	while (<F>) {
		chomp;
		if (/^ssh-(?:dss|rsa) /) {
			push @keys, $_;
		} elsif (/^# REPOAUTH ([0-9a-f]+) (\d+)/) {
			my $expire = $2;
			$auth = $1 unless (time >= $expire);
		}
	}
	close F;
	my $keys = join('', @keys); chomp $keys;
	($keys, $auth);
}

sub _sshkey_save {
	my $self = shift;
	open F, ">".jailed_file($self->_sshkey_path) or die "sshkey failed: $!";
	if (defined($self->{auth}) && $self->{auth}) {
		my $expire = time + 24 * 3600;
		print F "# REPOAUTH $self->{auth} $expire\n";
	}
	print F $self->{keys}."\n";
	close F;
	chmod 0664, jailed_file($self->_sshkey_path);
}

# private constructor, do not use
sub _new {
	my $class = shift;
	my ($name) = @_;
	Girocco::User::valid_name($name) or die "refusing to create user with invalid name ($name)!";
	my $proj = { name => $name };

	bless $proj, $class;
}

# public constructor #0
# creates a virtual user not connected to disk record
# you can conjure() it later to disk
sub ghost {
	my $class = shift;
	my ($name) = @_;
	my $self = $class->_new($name);
	$self;
}

# public constructor #1
sub load {
	my $class = shift;
	my ($name) = @_;

	open F, jailed_file("/etc/passwd") or die "user load failed: $!";
	while (<F>) {
		chomp;
		@_ = split /:/;
		next unless (shift eq $name);

		my $self = $class->_new($name);

		(undef, $self->{uid}, undef, $self->{email}) = @_;
		($self->{keys}, $self->{auth}) = $self->_sshkey_load;

		return $self;
	}
	close F;
	undef;
}

# public constructor #2
sub load_by_uid {
	my $class = shift;
	my ($uid) = @_;

	open F, jailed_file("/etc/passwd") or die "user load failed: $!";
	while (<F>) {
		chomp;
		@_ = split /:/;
		next unless ($_[2] eq $uid);

		my $self = $class->_new($_[0]);

		(undef, undef, $self->{uid}, undef, $self->{email}) = @_;
		($self->{keys}, $self->{auth}) = $self->_sshkey_load;

		return $self;
	}
	close F;
	undef;
}

# $user may not be in sane state if this returns false!
sub cgi_fill {
	my $self = shift;
	my ($gcgi) = @_;
	my $cgi = $gcgi->cgi;

	$self->{name} = $gcgi->wparam('name');
	Girocco::User::valid_name($self->{name})
		or $gcgi->err("Name contains invalid characters.");

	$self->{email} = $gcgi->wparam('email');
	valid_email($self->{email})
		or $gcgi->err("Your email sure looks weird...?");

	$self->keys_fill($gcgi);
}

sub keys_fill {
	my $self = shift;
	my ($gcgi) = @_;
	my $cgi = $gcgi->cgi;

	$self->{keys} = $cgi->param('keys');
	length($self->{keys}) <= 4096
		or $gcgi->err("The list of keys is more than 4kb. Do you really need that much?");
	foreach (split /\r?\n/, $self->{keys}) {
		/^ssh-(?:dss|rsa) .* \S+@\S+$/ or $gcgi->err("Your ssh key (\"$_\") appears to have invalid format (does not start by ssh-dss|rsa or does not end with @-identifier) - maybe your browser has split a single key to multiple lines?");
	}

	not $gcgi->err_check;
}

sub keys_save {
	my $self = shift;

	$self->_sshkey_save;
}

sub gen_auth {
	my $self = shift;

	$self->{auth} = Digest::SHA1::sha1_hex(time . $$ . rand() . $self->{keys});
	$self->_sshkey_save;
	$self->{auth};
}

sub del_auth {
	my $self = shift;

	delete $self->{auth};
}

sub conjure {
	my $self = shift;

	$self->_passwd_add;
	$self->_sshkey_save;
}

### static methods

sub valid_name {
	$_ = $_[0];
	/^[a-zA-Z0-9+._-]+$/;
}

sub does_exist {
	my ($name) = @_;
	Girocco::User::valid_name($name) or die "tried to query for user with invalid name $name!";
	(-e jailed_file("/etc/sshkeys/$name"));
}

sub resolve_uid {
	my ($name) = @_;
	$Girocco::Config::chrooted and undef; # TODO for ACLs within chroot
	scalar(getpwnam($name));
}


1;
